[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5559556.svg)](https://doi.org/10.5281/zenodo.5559556)
[![License](https://img.shields.io/badge/License-AGPL--3.0-informational)](https://www.gnu.org/licenses/agpl-3.0.html)

# ETH Zurich // [World at Play, Cosmogony vol.I](https://voluptas.arch.ethz.ch/program/summer-schools/) // [VOLUPTAS Professur Charbonnet Heiz](https://charbonnet-heiz.arch.ethz.ch/) // 21–26 June 2021 (online)

[DOWNLOAD THE GAME](https://codeberg.org/mononym/noise/releases)

## authors
- Julien Rippinger
- Raphael Ridder
- Jan Schweizer

## acknowledgements
- Coaching: Francisco Moura Veiga, Pippin Barr & Andri Gerber
- Inspiration: Noise in Variables (2020) by Adam El-Hamadeh & Nina Guyot
- Assets:
    - Ben Espesito // [First Person Drifter for Unity](http://www.torahhorse.com/index.php/portfolio/first-person-drifter-for-unity/)
    - 5thElement // [Little Robot](https://www.blendswap.com/blend/26868)
    - Satsuru // [Stellar Sky](https://assetstore.unity.com/packages/2d/textures-materials/sky/stellar-sky-99558)
    - NASA // [Antenna models](https://github.com/nasa/NASA-3D-Resources/tree/master/3D%20Models)

## under the hood
- gameplay [script](https://codeberg.org/mononym/noise/src/branch/master/Assets/Scripts/SwitchCamera.cs)
- drifter [scripts](https://codeberg.org/mononym/noise/src/branch/master/Assets/First%20Person%20Drifter%20Controller/Scripts)

## cite as

Rippinger, Julien, Ridder, Raphael, & Schweizer, Jan. (2021). Noise (v1.0). Zenodo. https://doi.org/10.5281/zenodo.5559556

```bib
@software{Rippinger2021,
    title = {Noise},
    author = {Rippinger, Julien and Ridder, Raphael and Schweizer, Jan},
    date = {2021-10-10},
    doi = {10.5281/zenodo.5559556},
    url = {https://zenodo.org/record/5559556},
    abstract = {Video game developed during the ETHZ VOLUPTAS summer school 2021.},
    organization = {{Zenodo}}
}
```